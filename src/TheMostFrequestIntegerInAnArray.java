import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TheMostFrequestIntegerInAnArray {
    public static void main(String[] args) {

        int[] arrayNumbers = {10, 20, 15, 26, 7, 7, 7, 8, 8, 8, 8, 8, 18, 9};

        findingMostFrequentNumberInTheArray(arrayNumbers);
    }


    public static void findingMostFrequentNumberInTheArray(int[] arrayNumbers) {

        HashMap<Integer, Integer> integers = new HashMap<>();

        for (int i = 0; i < arrayNumbers.length; i++) {
            if (integers.containsKey(arrayNumbers[i])) {
                integers.put(arrayNumbers[i], integers.get(arrayNumbers[i])+1);
            } else {
                integers.put(arrayNumbers[i], 1);
            }
        }


        Set<Map.Entry<Integer, Integer>> map = integers.entrySet();

        int frequency = 0;
        int element = 0;


        for (Map.Entry<Integer, Integer> m : map) {
            if (m.getValue() > frequency) {
                element = m.getKey();
                frequency = m.getValue();
            }
        }


        System.out.println("The Element is:" + element + "..... The Frequency Is:" + frequency);
    }


}


//Get an Array
//{10,20,15,26,7,7,18,9}