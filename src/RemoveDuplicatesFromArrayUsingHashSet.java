import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class RemoveDuplicatesFromArrayUsingHashSet {


    public static void main(String[] args) {

        String[] arrayOfStrings = {"shaik", "sudheer", "arif", "shaik", "ravi", "shaik", "sudheer"};
        System.out.println("The Given String Array Is:" );
        for (String array : arrayOfStrings) {
            System.out.print(array + " ");
        }

        System.out.println();
        System.out.println("The Given String array After removing the duplicates are:");
        for (String name : removeDuplicates(arrayOfStrings)) {
            System.out.print(name +" ");
        }

    }

    public static Set<String> removeDuplicates(String[] inputArray) {

        List<String> stringList = Arrays.asList(inputArray);

        Set<String> stringSet = new LinkedHashSet<>(stringList);

        return stringSet;

    }
}


//Get the Array whether it is array of integers or strings
// Add into the List and convert that list to set
// Print the Set
