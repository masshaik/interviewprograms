public class ReverseANumber {

    public static void main(String[] args) {

        int givenNumber = 356;

        System.out.println("The Given Number is:" + givenNumber);
        System.out.println("The Reverse Number for the given Number is:" + reverseANumber(givenNumber));

    }

    public static int reverseANumber(int givenNumber) {

        int number = givenNumber;
        int sum = 0;
        int reminder = 0;
        while (number != 0) {

            reminder = number % 10;
            sum = sum * 10 + reminder;
            number = number / 10;
        }
        return sum;
    }
}


//Reverse a Number
//Get a Reminder and multiply with 10 and add that sum to the reminder