import java.util.*;

public class TheFirstNonRepeatedCharecterInAString {

    public static void main(String[] args) {

        String name = "arifarkdikdkdidk";
        //s=2, a=3, h=2,i = 1, k =1 , m=1, t =1, n=1

        // System.out.println("The First Non Repeated Charecter is:" + firstNonRepeatedCharectersInaString(name));
        firstNonRepeatedCharectersInaString(name);

    }

    private static void firstNonRepeatedCharectersInaString(String name) {

        char[] chars = name.toCharArray();
        LinkedHashMap<Character, Integer> map = new LinkedHashMap();


        for (int i = 0; i < chars.length; i++) {
            if (map.containsKey(chars[i])) {
                map.put(chars[i], map.get(chars[i]) + 1);
            } else {
                map.put(chars[i], 1);
            }
        }

        System.out.println(map);

        Set<Map.Entry<Character, Integer>> entrySets = map.entrySet();

        for (Map.Entry<Character, Integer> entrySet : entrySets) {
            if (entrySet.getValue() == 1) {
                System.out.println(entrySet.getKey());
                break;
            }
        }
    }
}


// Take the Input String   -- shaikmasthan
// Find all the charecters along with the count
//