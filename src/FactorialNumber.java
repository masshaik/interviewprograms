public class FactorialNumber {
    public static void main(String[] args) {

        int givenNumber = 4;

        System.out.println("The Factorial for the Given number:" + givenNumber + " is:" + factorialNumber(givenNumber));

    }

    public static int factorialNumber(int givenNumber) {

        if (givenNumber == 1 || givenNumber == 0) {
            return 1;
        }

        int total = 1;

        for (int i = givenNumber; i > 1; i--) {
            total *= i;
        }
        return total;
    }
}


//Factorial Number
// 3 -> 3*2*1
// Get the Input
// In a loop Multiply with decriementing the number till 1