public class ReversedTheWordsInTheString {
    public static void main(String[] args) {

        String name = "shaik masthan is a java developer";

        reverseTheWordsInTheString(name);
    }

    private static void reverseTheWordsInTheString(String name) {

        String[] names = name.split(" ");

        for (int i = names.length - 1; i >= 0; i--) {
            System.out.print(names[i] + " ");
        }
    }
}


//Take the Given String
// shaik masthan is a java developer
// get the arrays by splitting like based on the spaces
// and print reverse of those by iterating