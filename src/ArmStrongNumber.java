public class ArmStrongNumber {

    public static void main(String[] args) {

        int number = 1531;
        System.out.println("The Given Number is ArmStrong Number:" + isArmStrongNumber(number));

    }


    public static boolean isArmStrongNumber(int number) {


        int givenNumber = number;

        int total = 0;
        int reminder = 0;

        while (number != 0) {

            reminder = number % 10;
            total = (int) (total + Math.pow(reminder, 3));

            number /= 10;
        }


        if (givenNumber == total) {
            return true;
        }
        return false;
    }
}


//ArmStrong Number
//Take the Input
// Get the Reminder and find the cube of every digit in the number
// and sum it
//verify that sum to the given number


