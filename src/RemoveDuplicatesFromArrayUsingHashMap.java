import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RemoveDuplicatesFromArrayUsingHashMap {

    public static void main(String[] args) {

        int[] arrayNumbers = {10, 39, 53, 83, 10, 39, 64, 873, 8, 93, 90, 31, 39, 53};
        for (int array : arrayNumbers) {
            System.out.print(array + " ");
        }

        System.out.println("The Given Array Is:" + arrayNumbers);
        System.out.println("After Removing the Duplicate from the Array is:");

        for (int array : removeDuplicatesFromAnArray(arrayNumbers)) {
            System.out.print(array + " ");
        }

    }

    public static Set<Integer> removeDuplicatesFromAnArray(int[] arrayNumbers) {


        HashMap<Integer, Integer> map = new HashMap<>();

        int count = 0;

        for (int i = 0; i < arrayNumbers.length; i++) {

            if (map.containsKey(arrayNumbers[i])) {
                map.put(arrayNumbers[i], map.get(arrayNumbers[i]) + 1);
            } else {
                map.put(arrayNumbers[i], 1);
            }
        }

        Set<Integer> integers = map.keySet();

        return integers;
    }

}


//Remove Duplicates From Array
// Get the Array of String or Integer
// Should Get the Count of Each Index Value in the Array
// If the count greater than 1 then it should become duplicate so remove that
//Using HashMap we can take the key as a indexvalue and value can be the count
//Get the KeySet from the map and print the set values