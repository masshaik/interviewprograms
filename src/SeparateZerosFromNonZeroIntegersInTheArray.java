import com.sun.tools.javac.util.ArrayUtils;

import java.util.Arrays;
import java.util.List;

public class SeparateZerosFromNonZeroIntegersInTheArray {
    public static void main(String[] args) {

        int[] separateZerosFromNonZeroIntegersInTheArray = {10, 20, 30, 40, 0, 12, 0, 28, 0, 1, 0};

        separateZerosFromNonZeroIntegersInTheArray(separateZerosFromNonZeroIntegersInTheArray);
        System.out.println(" ");
        separateZerosFromNonZeroIntegersInTheArrayToTheLeft(separateZerosFromNonZeroIntegersInTheArray);
    }

    private static void separateZerosFromNonZeroIntegersInTheArray(int[] array) {

        int temp = 0;

        for (int i = 0; i < array.length; i++) {

            for (int j = i + 1; j < array.length; j++) {

                if (array[i] == 0) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

    }


    private static void separateZerosFromNonZeroIntegersInTheArrayToTheLeft(int[] array) {

        int temp = 0;

        for (int i = 0; i < array.length; i++) {

            for (int j = i + 1; j < array.length; j++) {

               if(array[j] == 0){
                   temp = array[j];
                   array[j] = array[i];
                   array[i] = temp;
               }
            }
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

    }
}


//Get the given Array
//Get the each element in the array if the element value is zero then swap to the right
// 10, 20
// Iterate the first digit and next digit using two for loops and check if the number is equal to zero
// if it is equal we need to swap that 0 to either left or right based on the requirement
// For that we need to take some temp variable and assign to the index for which we wanted to swap with the other
// and assign the values
// print the array sorted