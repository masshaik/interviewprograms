public class RemoveDuplicatesOfAnArrayOfSorted {
    public static void main(String[] args) {

        int[] arrayOfIntegers = {0, 1, 1, 1, 2, 2, 2, 2, 2, 3, 4, 5, 6, 6, 7, 8, 8, 9, 10,12,12,12,13};

        removeDuplicatesOfAnArray(arrayOfIntegers);

    }

    private static void removeDuplicatesOfAnArray(int[] arrayOfIntegers) {

        int[] temp = new int[arrayOfIntegers.length];

        for (int i = 0; i < arrayOfIntegers.length-1; i++) {

            if (arrayOfIntegers[i] != arrayOfIntegers[i + 1]) {
                temp[i] = arrayOfIntegers[i];
                System.out.println(temp[i]);
            }
        }


    }
}


//Given Array {0,1,1,3,5,5,6,7,9,}
// Iterate Each Item and Take one temp array to insert unique elements
// and check if the nex element does not equal to the current index value then only insert into the array