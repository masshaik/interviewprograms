import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PrintRepeatedCharectersInString {
    public static void main(String[] args) {

        String name = "aaahs";
        printRepeatedCharetersInString(name);

    }

    private static void printRepeatedCharetersInString(String name) {
        char[] chars = name.toCharArray();

        HashMap<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < chars.length; i++) {
            if (map.containsKey(chars[i])) {
                map.put(chars[i], map.get(chars[i]) + 1);
            } else {
                map.put(chars[i], 1);
            }
        }

        Set<Map.Entry<Character, Integer>> entrySet = map.entrySet();
        for (Map.Entry<Character, Integer> entry : entrySet) {
            if (entry.getValue() > 1) {
                System.out.println(entry.getKey());
            }
        }

    }
}


//Get  a String
// Identify Which repeated more than once and print those chars