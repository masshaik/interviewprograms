public class SquareRootOfANumber {

    public static void main(String[] args) {

        int number = 4;

        printSquareRootOfNumber(number);
    }

    private static void printSquareRootOfNumber(int number) {

        System.out.println("The Square Root Of a Given Number is:" + (number * number));
    }
}
