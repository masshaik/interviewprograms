public class LeadersOfAnArray {
    public static void main(String[] args) {

        int[] givenArray = {10, 20, 49, 87, 29, 3, 9, 100};

        leadersOfAnArray(givenArray);

    }

    private static void leadersOfAnArray(int[] givenArray) {

        int max = givenArray.length-1;

        System.out.println("The Last Number always Higher:" + (givenArray.length-1));

        for(int i=givenArray.length-2; i>=0;i--){

            if(givenArray[i] > (max)){
                System.out.println("The Number:"+ givenArray[i]);
                max = givenArray[i];
            }
        }
    }
}


// Finding Leaders of An Array
// Get the Array {10,20,5,8,99,30,93}
// Iterate Each Digit with right to that and check if it is greater than to all the right side numbers
// If it is greater then print