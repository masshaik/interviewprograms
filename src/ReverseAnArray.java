public class ReverseAnArray {
    public static void main(String[] args) {


        int[] givenArray = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

        for(int number : givenArray){
            System.out.print(number);
        }
        System.out.println();

        printTheReverseOfAnArray(givenArray);
    }

    private static void printTheReverseOfAnArray(int[] givenArray) {

        int[] reversedArray = new int[givenArray.length];

        for (int i = givenArray.length - 1; i >= 0; i--) {
            System.out.print(givenArray[i] + " ");
        }

        System.out.println(" ");

        for (int i = givenArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < reversedArray.length; j++) {
                reversedArray[j] = givenArray[i];
                System.out.print(reversedArray[j]+ " ");
                break;
            }
        }


    }
}


//Get the Given Array
// iterate the Array from the last to first and add it to the other array or just print the reversed values or array