import java.util.Arrays;

public class FindingSecondLaregestOfIntegers {
    public static void main(String[] args) {
        int[] arrayOfNumbers = {10, 20, 0, 12, 15, 69, 90, 89, 100};

        secondLargestNumberOfAnArray(arrayOfNumbers);

    }

    public static void secondLargestNumberOfAnArray(int[] arrayOfNumbers) {


        int temp = 0;

        for (int i = 0; i < arrayOfNumbers.length; i++) {

            for (int j = 0; j < arrayOfNumbers.length; j++) {
                if (arrayOfNumbers[i] > arrayOfNumbers[j]) {

                    temp = arrayOfNumbers[i];
                    arrayOfNumbers[i] = arrayOfNumbers[j];
                    arrayOfNumbers[j] = temp;
                }
            }
        }


        System.out.println(arrayOfNumbers[1]);
    }


}


//Get the Given Array
//{10,20,0,12,15,69,90,89,100}
// first digit compare with all the digits in the array if it is higher then store somewhere
// and get the second digit from the stored array