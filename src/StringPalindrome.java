import com.sun.tools.javac.util.StringUtils;

public class StringPalindrome {
    public static void main(String[] args) {

        String input = "shahsa";
        System.out.println("The Given String  " + input + " is palindrome:" + isPalindrome(input));

    }

    public static boolean isPalindrome(String input) {

        char[] chars = input.toCharArray();

        StringBuffer stringBuffer = new StringBuffer();

        for (int ch = chars.length - 1; ch >= 0; ch--) {
            stringBuffer.append(chars[ch]);
        }
        System.out.println("reversed string is :" + stringBuffer);

        if (input.equalsIgnoreCase(stringBuffer.toString())) {
            return true;
        } else {
            return false;
        }
    }
}


//Take the Input String
//Convert to Char Array
//Put Each Charecter in the StringBuffer
//Match Whether given string and reversed string both are equal