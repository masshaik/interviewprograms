import java.util.Arrays;

public class AnagramString {
    public static void main(String[] args) {

        String givenString = "shaik";

        String anagram = "haisk";


        char[] givenStringChars = givenString.toCharArray();
        char[] anagramChars = anagram.toCharArray();


         Arrays.sort(givenStringChars);
         Arrays.sort(anagramChars);


        System.out.println("The Both Strings are equal: " +Arrays.equals(givenStringChars,anagramChars));

    }

}

//Anagram String
//Sort the Two Strings
//and verify if both are equal or not