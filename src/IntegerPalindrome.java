public class IntegerPalindrome {
    public static void main(String[] args) {

        int givenNumber = 313;

        System.out.println("The Given Number is Palindrome or not   " + givenNumber + "    " + isPalindrome(givenNumber));


    }

    public static boolean isPalindrome(int givenNumber) {


        int total = 0;
        int reminder = 0;

        int number = givenNumber;

        while (givenNumber != 0) {

            reminder = givenNumber % 10; //3 3 5
            total = total * 10 + reminder; //3 33
            givenNumber = givenNumber / 10; //35 5
        }

        if (total == number) {
            return true;
        }
        return false;
    }
}


//353
//Take the Input Integer
//Get the Reminder along with that add the multiplication with 10 for each reminder digit
//0*10+3
