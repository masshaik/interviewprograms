public class MissingNumberFindOutInTheArray {
    public static void main(String[] args) {

        int[] numbers = {1, 4, 5, 3, 7, 8, 6}; //3
        int number = 8;
        findOutTheMissingNumberInTheArray(numbers, number);

    }

    private static void findOutTheMissingNumberInTheArray(int[] numbers, int n) {

        int totalSum = (n * (n + 1)) / 2;
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        System.out.println("The number which is missing:" + (totalSum - sum));
    }


}

// Take the Input Array and the size of the array
// Find Out the Total Sum of the Array Elements with the formula (n(n+1)/2)
// Find the Total Sum of the Elements that are there in the array
// Substract both sums and we will get the missing number
