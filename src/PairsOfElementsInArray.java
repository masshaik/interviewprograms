public class PairsOfElementsInArray {
    public static void main(String[] args) {

        int[] pairs = {5, 4, 3, 6, 9, 0, 1, 8, 7, 2};

        int sum = 9;

        pairsOfElementsInArray(pairs, sum);
    }

    private static void pairsOfElementsInArray(int[] pairs, int sum) {

        for (int i = 0; i < pairs.length; i++) {

            for (int j = i + 1; j < pairs.length; j++) {

                if ((pairs[i] + pairs[j]) == sum) {
                    System.out.println("The Sum of the Pairs Of the Given Digits are:" + pairs[i] + "+" + pairs[j] + "=" + (pairs[i] + pairs[j]));
                }
            }
        }

    }
}


//Take the Given Array
// Iterate Each Number in an Array with the other number which is next to that
// and Check that sum is the given number then add those pairs
//Print