public class ReverseAString {

    public static void main(String[] args) {

        String name = "shaikmasthan";

        System.out.println("The Given String is:" + name);

        System.out.println("The Reverse Of a Given String is:" + reverseOfString(name));

    }

    private static String reverseOfString(String name) {

        char[] chars = name.toCharArray();

        StringBuffer stringBuffer = new StringBuffer();
        for (int i = chars.length-1; i >=0.; i--) {
            stringBuffer.append(chars[i]);
        }
        return stringBuffer.toString();
    }
}
