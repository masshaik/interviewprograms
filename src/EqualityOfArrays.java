public class EqualityOfArrays {
    public static void main(String[] args) {

        int[] arrayOne = {1, 3, 5, 6};
        int[] arrayTwo = {1, 3, 5, 6};

        System.out.println("The Both Arrays are:" + findTheEqualityOfTwoArrays(arrayOne, arrayTwo));
    }

    private static boolean findTheEqualityOfTwoArrays(int[] arrayOne, int[] arrayTwo) {

        boolean isEqual = true;


        if(arrayOne.length == arrayTwo.length){

            for(int i=0;i<arrayOne.length;i++){
                if(arrayOne[i] != arrayTwo[i]){
                    isEqual = false;
                }
            }
        }
        return  isEqual;

    }
}

//Get the Arrays
//Pass the Arrays
// Get each element of the array of both of same index and check the values of both

